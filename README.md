# GNOME Crosswords Contrib Puzzle Set

This package is for collecting the great puzzles put out by crossword
authors. If you're interested in contributing, please feel to open a
merge request or [let me know](mailto:jrb@gnome.org).

Alternatively, feel free to drop by #crosswords on matrix. You can
join the chat room [here](https://matrix.to/#/#crosswords:gnome.org).

## Building and Testing

There are two ways to test this puzzle-set:

### meson
If you're running Crosswords from source, you should build this
repository with meson.

```shell
$ meson setup _build
$ ninja -C _build
```

Then, go to your _build of Crosswords:

```shell
$ PUZZLE_SET_PATH=~/path/to/puzzle-sets-gnome/_build/puzzle-sets/ ./run src/crosswords
```

### flatpak

Alternatively, you run Crosswords from flatpak (Devel or from
flathub), you install a local flatpak of this puzzle set:

```shell
# flatpak-builder  --force-clean _flatpak/ org.gnome.Crosswords.PuzzleSets.gnome.json  --user --install
```
