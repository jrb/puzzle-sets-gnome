#!/usr/bin/python
import os
import fnmatch
from bs4 import BeautifulSoup
import regex
import re
import unicodedata

wget_command='wget -r -A \.puz https://mayaofauckland.com/Alphacross'
ipuz_convertor='~/Projects/crosswords/_build/run ~/Projects/crosswords/_build/tools/ipuz-convertor -i %s -o %s -l CC-BY-SA-4.0'
basedir='./mayaofauckland.com/Alphacross/'
exceptions={}
exceptions['moon.puz']='moon2.puz'
skips=['That\'ll do nicely.puz', 'Moon.puz', '068.puz', 'SubStack26.puz']
def slugify(value, allow_unicode=False):
    """
    Taken from https://github.com/django/django/blob/master/django/utils/text.py
    Convert to ASCII if 'allow_unicode' is False. Convert spaces or repeated
    dashes to single dashes. Remove characters that aren't alphanumerics,
    underscores, or hyphens. Convert to lowercase. Also strip leading and
    trailing whitespace, dashes, and underscores.
    """
    value = str(value)
    if allow_unicode:
        value = unicodedata.normalize('NFKC', value)
    else:
        value = unicodedata.normalize('NFKD', value).encode('ascii', 'ignore').decode('ascii')
    value = re.sub(r'[^\w\s-]', '', value.lower())
    return re.sub(r'[-\s]+', '-', value).strip('-_')



if __name__ == '__main__':
    config = open('./puzzle.config', "w")
    config.write ("""# Māyā of Aukland
[Puzzle Set]
ID=mayaofauckland
ShortName=Māyā of Aukland
LongName=Cryptic crosswords by Māyā
Picker=list
Locale=en_NZ
Disabled=false
Tags=regular;cryptic;

[Picker List]
Header=Māyā of Aukland
SubHeader=Cryptic crosswords by Māyā. Updated every couple of weeks, or more often if inspired.
HeaderFace=Roboto
SubHeaderFace=Roboto
ShowProgress=true
URL=https://mayaofauckland.substack.com/

""")

    gresource = open('../mayaofauckland.gresource.xml', "w")
    gresource.write ("""<?xml version="1.0" encoding="UTF-8"?>
<gresources>
  <gresource prefix="/org/gnome/Crosswords/puzzle-set/">
    <file>mayaofauckland/puzzle.config</file>
""")

    #Uncomment this to get new puzzles
    os.system (wget_command)
    puznum=1
    for root, subdir, files in os.walk(basedir):
        for f in files:
            if f.endswith (".puz"):
                if f in skips:
                    print ("skipping %s" % f)
                    continue
                source = os.path.join (root, f)
                try:
                    f = exceptions[f]
                except:
                    pass
                target = slugify (f[:-4])
                if not os.path.exists (target + ".ipuz"):
                    os.system (ipuz_convertor % (source, target + ".ipuz"))
                config.write('[Puzzle%d]\n' % puznum)
                config.write('PuzzleName=%s.ipuz\n\n' % target)
                gresource.write ('    <file preprocess="json-stripblanks">mayaofauckland/%s.ipuz</file>\n' % target)
                puznum += 1
    gresource.write ("""  </gresource>
</gresources>
""")

